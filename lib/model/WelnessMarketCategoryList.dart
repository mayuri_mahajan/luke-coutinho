class WelnessMarketCategoryList {
  String CategoryName = "";
  bool IsSelected = false;

  WelnessMarketCategoryList(String _categoryName, bool _isSelected) {
    this.CategoryName = _categoryName;
    this.IsSelected = _isSelected;
  }

  void setSelected(bool value) {
    this.IsSelected = value;
  }

  bool getSelected() {
    return IsSelected;
  }
}
