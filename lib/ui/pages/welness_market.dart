import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:luke_catinho/app/Constants.dart' as Constants;
import 'package:luke_catinho/model/WelnessMarketCategoryList.dart';
import 'package:luke_catinho/model/WelnessMarketProduct.dart';
import 'package:luke_catinho/ui/widgets/WelnessMarketFeaturedProducts.dart';
import 'package:luke_catinho/ui/widgets/WelnessMarketListItem.dart';
import 'package:luke_catinho/ui/widgets/WelnessMarketListItems.dart';
import 'package:luke_catinho/ui/widgets/WelnessMarketLukeRecommends.dart';

void main() {
  runApp(WelnessMarket());
}

List<WelnessMarketProducts> welnessProductList = [
  new WelnessMarketProducts(
      "Health and Lifestyle",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fhealth_and_lifestyle.png?alt=media&token=ce62cd96-d622-4da3-bc8c-8fe00c011cf0",
      "FFE7D0"),
  new WelnessMarketProducts(
      "Personal care and Beauty",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fpersonal_care.png?alt=media&token=0ff8b7f2-994f-437d-951e-5a6eee454e07",
      "F5C8F9"),
  new WelnessMarketProducts(
      "Kids and baby Care",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fkids_care.png?alt=media&token=8891882e-42b3-429c-a648-8b1d9d88c920",
      "C4E0F6"),
  new WelnessMarketProducts(
      "Health and Lifestyle",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fhealth_and_lifestyle.png?alt=media&token=ce62cd96-d622-4da3-bc8c-8fe00c011cf0",
      "FFE7D0"),
  new WelnessMarketProducts(
      "Personal care and Beauty",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fpersonal_care.png?alt=media&token=0ff8b7f2-994f-437d-951e-5a6eee454e07",
      "F5C8F9")
];
List<WelnessMarketCategoryList> welnessMarketCategoryList = [
  new WelnessMarketCategoryList('Pulses & Cerels', true),
  new WelnessMarketCategoryList('Vegetables', false),
  new WelnessMarketCategoryList('Kitchenware', false),
  new WelnessMarketCategoryList('Pulses & Cerels', false),
  new WelnessMarketCategoryList('Vegetables', false),
  new WelnessMarketCategoryList('Kitchenware', false),
];

class WelnessMarket extends StatefulWidget {
  WelnessMarket({Key key}) : super(key: key);

  @override
  _WelnessMarket createState() => _WelnessMarket();
}

class _WelnessMarket extends State<WelnessMarket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Color(0xFFF7F7F7),
        appBar: AppBar(
          toolbarHeight: 100,
          leadingWidth: 0,
          titleSpacing: 0,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 10),
            decoration: new BoxDecoration(
              color: Color(0xFFf7f7f7),
              boxShadow: [
                new BoxShadow(
                    color: Color(0xFF000000).withOpacity(0.08),
                    offset: Offset(0, 4), //(x,y)
                    blurRadius: 25.0)
              ],
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    flex: 1,
                    child: Stack(
                      /*mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,*/
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                          child: Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(top: 8, right: 10),
                              padding: EdgeInsets.only(right: 25),
                              decoration: new BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                  topLeft: Radius.circular(5),
                                ),
                              ),
                              //child: Text('hellos'),
                              child: TextField(
                                cursorColor: Color(0xFF888888),
                                autocorrect: true,
                                decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding:
                                      EdgeInsets.fromLTRB(12, 8, 5, 8),
                                  fillColor: Colors.transparent,
                                  filled: true,
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 50.0,
                          height: 50.0,
                          padding: EdgeInsets.all(2),
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(50.0)),
                          ),
                          child: Image.asset(
                            'assets/images/luke_market_logo.png',
                            height: 50,
                            width: 50,
                          ),
                        ),
                        Positioned(
                          right: 5,
                          top: 15,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(5.0, 0, 14.0, 1),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Image.asset(
                                'assets/images/ic_search.png',
                                height: 18,
                                width: 18,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                Padding(
                  padding: const EdgeInsets.only(right: 8, top: 5),
                  child: new Container(
                    width: 35.0,
                    height: 35.0,
                    decoration: new BoxDecoration(
                      color: Color(0xFFFF9C01).withOpacity(0.12),
                      boxShadow: [
                        new BoxShadow(
                            color: Color(0xFFFF9C01).withOpacity(0.2),
                            offset: Offset(0, 6), //(x,y)
                            blurRadius: 12.0)
                      ],
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(35.0)),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(7),
                      child: Image.asset(
                        'assets/images/ic_like.png',
                        height: 35,
                        width: 35,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: new Container(
                    width: 35.0,
                    height: 35.0,
                    decoration: new BoxDecoration(
                      color: Color(0xFFFF9C01).withOpacity(0.12),
                      boxShadow: [
                        new BoxShadow(
                            color: Color(0xFFFF9C01).withOpacity(0.2),
                            offset: Offset(0, 6), //(x,y)
                            blurRadius: 12.0)
                      ],
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(35.0)),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(7),
                      child: Image.asset(
                        'assets/images/ic_cart.png',
                        height: 35,
                        width: 35,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                // mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 110.0,
                    maxHeight: 110.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: welnessProductList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        welnessMarketProductListItems(context, index),
                    // buildBody(context, index),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 95.0,
                    maxHeight: 95.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: welnessProductList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        welnessMarketListItems(context, index),
                    // buildBody(context, index),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(12, 25, 10, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          Constants.Strings.LUKE_RECOMMENDS,
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            Constants.Strings.VIEW_ALL,
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xFFFF9C01),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/ic_forword_arrow.png',
                              height: 10,
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          ),
                        ],
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 10),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 230.0,
                    maxHeight: 230.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: welnessProductList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        welnessMarketlukeRecommends(context, index),
                    // buildBody(context, index),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(12, 0, 10, 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          Constants.Strings.FEATURED_PRODUCTS,
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            Constants.Strings.VIEW_ALL,
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xFFFF9C01),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/ic_forword_arrow.png',
                              height: 10,
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          ),
                        ],
                      ),
                    ],
                  )),
                  ConstrainedBox(
                    constraints: new BoxConstraints(
                      minHeight: 255.0,
                      maxHeight: 265.0,
                    ),
                child: CarouselSlider(
                  options: CarouselOptions(
                    height: 260,
                    viewportFraction: 0.5,
                    initialPage: 0,
                    // aspectRatio: 10/9,
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    enableInfiniteScroll: true,
                    reverse: false,
                    autoPlay: true,
                    disableCenter: false,
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                  ),
                  items: welnessProductList.map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return welnessMarketFeaturedProduct(
                            context, welnessProductList.indexOf(i));
                      },
                    );
                  }).toList(),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  height: 30,
                  margin: EdgeInsets.only(top:10),
                  child: ListView.builder(
                      itemCount: welnessMarketCategoryList.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return WelnessMarketCategory(index);
                      }), /*ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: welnessMarketCategoryList.length,
                        itemBuilder: (BuildContext context, int index) =>
                            welnessMarketProducts(context, index),
                        // buildBody(context, index),
                      ),*/
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 0, 10),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 225.0,
                    maxHeight: 225.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: welnessProductList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        welnessMarketlukeRecommends(context, index),
                    // buildBody(context, index),
                  ),
                ),
              ),
              Container(
                height: 130,
                //smargin: EdgeInsets.only(top: 5),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                          height: 85,
                          width: double.infinity,
                          margin: EdgeInsets.fromLTRB(50, 5, 12, 0),
                          decoration: BoxDecoration(
                            color: Color(0xFFD3F57D),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              new BoxShadow(
                                  color: Color(0xFF000000).withOpacity(0.08),
                                  offset: Offset(0, 4),
                                  blurRadius: 15.0)
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 120.0, right: 10, bottom: 5),
                                child: Text(
                                  'Become our Seller today',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'Gotham Rounded',
                                      fontStyle: FontStyle.normal,
                                      color: Colors.black,
                                      fontSize: 14),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 120, top: 3, right: 10),
                                child: Text(
                                  'Showcase your products on Wellness Store',
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontFamily: 'Gotham Rounded',
                                      fontStyle: FontStyle.normal,
                                      color: Colors.black,
                                      fontSize: 12),
                                ),
                              ),
                            ],
                          )),
                    ),
                    Positioned(
                      child: CachedNetworkImage(
                        imageUrl:
                            'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2FIllustration.png?alt=media&token=712cd9d7-a75e-4d84-969d-f539940dd5f9',
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: 130,
                        width: 160,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(12, 20, 12, 5),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Why Luke’s Holistic Health Store?',
                    maxLines: 2,
                    style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Fotham Rounded',
                      fontSize: 14,
                      color: Color(0xFF000000),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 5),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Reasons why you should consider buying from wellness store',
                    maxLines: 2,
                    style: TextStyle(
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w300,
                      fontFamily: 'Fotham Rounded',
                      fontSize: 12,
                      color: Color(0xFF000000),
                    ),
                  ),
                ),
              ),
            ])));
  }

/* Widget get listTile {
    return
  }*/
}

class WelnessMarketCategory extends StatefulWidget {
  final int index;

  const WelnessMarketCategory(this.index);

  // WelnessMarketCategory(int index, {Key key}) : super(key: key);

  @override
  _WelnessMarketCategory createState() => _WelnessMarketCategory(index);
}

class _WelnessMarketCategory extends State<WelnessMarketCategory> {
  // This is for individual ListTile reference
  //bool isSelected = false;
 // int index = 0;
  _WelnessMarketCategory(int index);

  @override
  Widget build(BuildContext context) {
    /*if(welnessMarketCategoryList[widget.index].IsSelected){
      isSelected=true;

    }else{
      isSelected=false;

    }*/
    return Container(
      // width: 130,
      height: 30,
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => (showWidget(true,widget.index)),
              child: Text(
                welnessMarketCategoryList[widget.index].CategoryName,
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'Gotham Rounded',
                  fontSize: 14,
                  color: Color(0xFF333333),
                  // decoration: TextDecoration.underline,
                ),
              ),
            ),
            // for each listtile individually, it shows/hide Row accordingl
            Visibility(
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                visible: welnessMarketCategoryList[widget.index].IsSelected,
                child: Container(
                  color: Color(0xFFFF9C01),
                  width: 100,
                  height: 1,
                )),
          ]),
    );
  }

  void showWidget(bool value,int index) {
    setState(() {
      for(int a=0;a<welnessMarketCategoryList.length;a++) {
        print('showWidget: selected  widget ${widget.index}  $value  ${welnessMarketCategoryList[a].IsSelected}  ${welnessMarketCategoryList.indexOf(welnessMarketCategoryList[a]) }');

        if(index==welnessMarketCategoryList.indexOf(welnessMarketCategoryList[a])) {
          welnessMarketCategoryList[a].setSelected(true);
        }else{
          welnessMarketCategoryList[a].setSelected(false);

        }
      }
    });
  }
}
