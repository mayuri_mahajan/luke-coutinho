import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget helthyRecipieListItem(BuildContext ctxt, int index) {
  return Container(
    //decoration: new BoxDecoration(color: Colors.white),
    // height: 230,
    width: 260,
    margin: EdgeInsets.fromLTRB(0.0, 0, 8.0, 0.0),
    child: Stack(
      children: <Widget>[
        Container(
          width: 210.0,
          // height: 240,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: Color(0xFFFFDFD0),
          ),
          padding: EdgeInsets.fromLTRB(12, 10, 10, 10),
          child: Wrap(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Wrap(
                    children: [
                      Card(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            side:
                                BorderSide(width: 1, color: Color(0xFFF69365))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 17),
                          child: Text(
                            'Cuisine: Italian ',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 24, right: 50),
                    child: Text(
                      'Chicken Cheese Burger',
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Text(
                      'Serves: 2',
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      'Time: 10min ',
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0, bottom: 10),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 12.0),
                          child: Text(
                            'Recipe',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w300,
                              //   fontWeight: FontWeight.lerp(FontWeight.w300, FontWeight.w400, 0.25)
                            ),
                          ),
                        ),
                        Container(
                          width: 28.0,
                          height: 28.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              new BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0.0, 1.0),
                                  //(x,y)
                                  blurRadius: 10.0)
                            ],
                            image: new DecorationImage(
                              image: new NetworkImage(''),
                              fit: BoxFit.cover,
                            ),
                            borderRadius:
                                new BorderRadius.all(new Radius.circular(28.0)),
                            border: new Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset(
                                'assets/images/ic_forword_arrow_red.png'), /*mage.asset(
                                'assets/images/ic_forword_arrow_red.png'),*/
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Positioned(
            right: -5,
            bottom: 10,
            child: CachedNetworkImage(
              imageUrl:
                  "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fburger.png?alt=media&token=7695f15c-d82d-4a5e-96af-405895b955b7",
              //  placeholder: (context, url) => CircularProgressIndicator(),
              height: 140,
              width: 155,
              errorWidget: (context, url, error) => Icon(Icons.error),
            )
            /*Image.asset(
            'assets/images/burger.png',
            height: 140,
            width: 170,
          ),*/
            )
      ],
    ),
  );
}
