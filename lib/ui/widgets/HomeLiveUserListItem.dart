import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luke_catinho/main.dart';

Widget liveUserListItems(BuildContext ctxt,int index) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(3, 0, 3, 0),
    child: new Container(
      width: 42.0,
      height: 42.0,
      decoration: new BoxDecoration(
        color: Colors.white,
        boxShadow: [
          new BoxShadow(
              color: Color(0xFF000000).withOpacity(0.2),
              offset: Offset(0, 6), //(x,y)
              blurRadius: 12.0)
        ],
        image: new DecorationImage(
          image: new NetworkImage(''),
          fit: BoxFit.cover,
        ),
        borderRadius: new BorderRadius.all(new Radius.circular(42.0)),
        border: new Border.all(
          color: Colors.white,
          width: 1.5,
        ),
      ),
      child: Container(
        child: CachedNetworkImage(
          imageUrl: pastLiveSessionsUser[index],
          //placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    ),
  );
}
