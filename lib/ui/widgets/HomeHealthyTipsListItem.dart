import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget helthyTipsListItem(BuildContext ctxt, int index) {
  return Container(
    width: double.infinity,
    height: 250,
    /*padding: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
    decoration: new BoxDecoration(
      boxShadow: [
        new BoxShadow(
            color: Color(0XFF000000).withOpacity(0.03),
            offset: Offset(0, 4),
            //(x,y)
            blurRadius: 15.0)
      ],
    ),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          new BoxShadow(
              color: Color(0XFFCECECE).withOpacity(0.3),
              offset: Offset(0, 4),
              //(x,y)
              blurRadius: 15.0)
        ],
      ),
      margin: EdgeInsets.fromLTRB(13, 13, 13, 13),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 130.0,
            margin:
            EdgeInsets.symmetric(vertical: 8, horizontal: 8),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(8.0),
              color: const Color(0xff000000).withOpacity(0.1),
            ),
            child: CachedNetworkImage(
              imageUrl:
              'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fhealthy_tip.png?alt=media&token=246be8d8-3ae2-4964-ae07-26be05a05f66',
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(8.0),
                  color: const Color(0xff000000),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.7),
                        BlendMode.dstATop),
                  ),
                ),
              ),
              // placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) =>
                  Icon(Icons.error),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(9, 12, 8, 5),
            child: Text(
              'The Sugar and Cancer Connection',
              style: TextStyle(
                fontSize: 17,
                fontFamily: 'Ubantu',
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(9, 4, 8, 20),
            child: Text(
              'This is what you should do to feel energised all day long.',
              style: TextStyle(
                fontSize: 12,
                fontFamily: 'Ubantu',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w300,
              ),
            ),
          )
        ],
      ),
    ),
  );
}
