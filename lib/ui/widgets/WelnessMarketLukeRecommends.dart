import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget welnessMarketlukeRecommends(BuildContext ctxt, int index) {
  return Container(
     margin: EdgeInsets.fromLTRB(5, 0, 5, 15),
    width: 150,
   // height: 300,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
      boxShadow: [
        new BoxShadow(
            color: Color(0xFF000000).withOpacity(0.07),
            offset: Offset(0, 4),
            blurRadius: 15.0)
      ],
    ),
    /*padding: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
    child: Column(
      children: [
        Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    new BoxShadow(
                        color: Color(0xFFFFFFFF).withOpacity(0.8),
                        offset: Offset(0, 4),
                        blurRadius: 15.0)
                  ],
                ),
                child: CachedNetworkImage(
                  imageUrl:
                      'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fcoconut_oil.png?alt=media&token=8da9a6d0-2be7-4e9b-99ea-b7e71dcb4102',
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  height: 140,
                  width: 150,
                ),
              ),
            ),
            Positioned(
              right: 10,
              top: 20,
              child: Container(
                padding: EdgeInsets.all(4),
                height: 28,
                width: 28,
                decoration: new BoxDecoration(
                  color: Color(0xFFFFFFFF),
                  borderRadius:
                      new BorderRadius.all(new Radius.circular(28.0)),
                ),
                child: Image.asset(
                  'assets/images/ic_like.png',
                ),
              ),
            )
          ],
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10,top:10,right: 5),
                  child: Text(
                    'Coconut Oil',
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontFamily: 'Gotham Rounded',
                        fontStyle: FontStyle.normal),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10,top:4,right: 5),
                  child: Text(
                    'Sold By ABC',
                    style: TextStyle(
                        fontSize: 9,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                        fontFamily: 'Gotham Rounded',
                        fontStyle: FontStyle.normal),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left:10.0,bottom: 5,top:7),
                        child: Text(
                          '₹ 790.00/-',
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                              fontFamily: 'Gotham Rounded',
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/images/ic_rectangle_cart.png',
                      height: 27,
                      width: 27,
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    ),
  );
}
