import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget lukeTalksListItems(BuildContext ctxt, int index) {
  return Container(
    width: 250,
    height: 250,
    /*decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
      boxShadow: [
        new BoxShadow(
            color: Color(0xFFEEEEEE).withOpacity(1),
            offset: Offset(10, 0.0),
            //(x,y)
            blurRadius: 10.0)
      ],
    ),*/
    /*padding: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          new BoxShadow(
              color: Color(0XFFCECECE).withOpacity(0.5),
              offset: Offset(0, 4),
              //(x,y)
              blurRadius: 15.0)
        ],
      ),
      margin: EdgeInsets.fromLTRB(13, 12, 4, 15),
      // elevation: 5,
      /*shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),*/
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: CachedNetworkImage(
                imageUrl:
                    "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fluketalks.png?alt=media&token=edc76d40-a13f-43d5-ae79-1a23aa6d6959",
                height: 120,
                width: double.infinity,
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 5, 8, 2),
            child: Text(
              'Yoga Pranayam',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
            child: Row(
              children: [
                Image.asset(
                  'assets/images/ic_videoplay.png',
                  height: 35,
                  width: 35,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    '4 Videos',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
