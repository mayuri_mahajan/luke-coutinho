import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luke_catinho/ui/pages/welness_market.dart';

Widget welnessMarketProductListItems(BuildContext ctxt, int index) {
  return Container(
    width: 125,
    height: 110,
    margin: EdgeInsets.fromLTRB(5, 5, 5, 15),
    padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
    decoration: BoxDecoration(
      color: colorConvert(welnessProductList[index].productColorCode),
      borderRadius: BorderRadius.circular(8),
      boxShadow: [
        new BoxShadow(
            color: Color(0xFF000000).withOpacity(0.07),
            offset: Offset(0, 4),
            blurRadius: 15.0)
      ],
    ),
    /*padding: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
    child: Column(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(top:0),
            child: CachedNetworkImage(
              imageUrl: welnessProductList[index].productImage,
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: 50,
              width: 50,
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  welnessProductList[index].productName,
                  style: TextStyle(fontSize: 12, color: Colors.black,fontFamily: 'Gotham Rounded',fontStyle: FontStyle.normal),

                ),
              ),
            )),
      ],
    ),
  );

}
Color colorConvert(String color) {
  color = color.replaceAll("#", "");
  if (color.length == 6) {
    return Color(int.parse("0xFF"+color));
  } else if (color.length == 8) {
    return Color(int.parse("0x"+color));
  }
}
