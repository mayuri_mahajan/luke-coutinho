import 'package:flutter/cupertino.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:luke_catinho/main.dart';
import 'package:flutter/material.dart';


Widget buildBody(BuildContext ctxt, int index) {
  return Container(
    //decoration: new BoxDecoration(color: Colors.white),
    padding: EdgeInsets.fromLTRB(4.0, 0, 5.0, 5.0),
    child: Stack(
      children: <Widget>[
        Container(
          height: 175.0,
          width: 140.0,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(8.0),
            color: const Color(0xff000000).withOpacity(0.1),
            /*image: new DecorationImage(
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.7), BlendMode.dstATop),
            image: new AssetImage(
              'assets/images/sample2.png',
            ),
          ),*/
          ),
          child: CachedNetworkImage(
            imageUrl: userList[index].backImage,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(8.0),
                color: const Color(0xff000000),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.7), BlendMode.dstATop),
                ),
              ),
            ),
            // placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
        Positioned(
          bottom: 12,
          right: 43,
          child: new Container(
            width: 50.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: const Color(0xFFFBB03A),
              boxShadow: [
                new BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.2),
                    blurRadius: 12,
                    offset: Offset(0.0, 6)),
              ],
              image: new DecorationImage(
                image: new NetworkImage(''),
                fit: BoxFit.cover,
              ),
              borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
              border: new Border.all(
                color: Colors.white,
                width: 1.5,
              ),
            ),
            child: Container(
              child: CachedNetworkImage(
                imageUrl: userList[index].userProfileImage,
                // placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) =>
                    Image.asset('assets/images/ic_add.png'),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}