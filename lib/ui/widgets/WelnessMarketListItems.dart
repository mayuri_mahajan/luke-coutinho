import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget welnessMarketListItems(BuildContext ctxt, int index) {
  return Container(
    height: 90,
    width: 300,
    margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
    padding: EdgeInsets.fromLTRB(5, 5, 10, 5),
    decoration: BoxDecoration(
      color: Color(0xFFFFE7D0),
      borderRadius: BorderRadius.circular(8),
      boxShadow: [
        new BoxShadow(
            color: Color(0xFF000000).withOpacity(0.07),
            offset: Offset(0, 4),
            blurRadius: 15.0)
      ],
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 2,
          child: Text('We Desi: Traditional Classics With a Healthy Twist',
              maxLines: 3,
              style: TextStyle(
                  fontSize: 13,
                 color: Colors.black,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Gotham Rounded')),
        ),
        Expanded(
          flex: 1,
          child: CachedNetworkImage(
            imageUrl:
                'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Fwelness_market_items.png?alt=media&token=c54a73eb-d02e-4e71-9161-07b91e87ab9f',
            errorWidget: (context, url, error) => Icon(Icons.error),
            height: 90,
            width: 90,
          ),
        )
      ],
    ),
  );
}
