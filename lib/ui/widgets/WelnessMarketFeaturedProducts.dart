import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luke_catinho/ui/pages/welness_market.dart';

Widget welnessMarketFeaturedProduct(BuildContext ctxt, int index) {
  return Container(
    // margin: EdgeInsets.fromLTRB(5, 15, 5, 15),
    // ing: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
    width: 180,
    height: 245,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
    //  color: Color(0xFFF7F7F7),

      boxShadow: [
        new BoxShadow(
            color: Color(0xFFC5C5C5).withOpacity(0.15),
            offset: Offset(0, 2),
            blurRadius: 10.0)
      ],
    ),
    child: Column(
      children: [
        Container(
          width: 180,
         // color: Color(0xFFF7F7F7),
          child: Stack(
            children: [
              Container(
                width: 170,
                height: 140,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    new BoxShadow(
                        color: Color(0xFF000000).withOpacity(0.09),
                        offset: Offset(0, 4),
                        blurRadius: 15.0)
                  ],
                ),
                child: CachedNetworkImage(
                  imageUrl:
                      'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2FWelnessMarket%2Ffeatured_products.png?alt=media&token=9ceead46-f3aa-4918-869e-235b19320b15',
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  height: 130,
                  width: 170,
                    fit: BoxFit.fill
                ),
              ),
              Positioned(
                right: 0,
                bottom: 30,
                child: Container(
                  padding: EdgeInsets.all(5),
                  height: 30,
                  width: 30,
                  decoration: new BoxDecoration(
                    color: Color(0xFFFF9C01),
                    borderRadius:
                        new BorderRadius.all(new Radius.circular(30.0)),
                  ),
                  child: Image.asset(
                    'assets/images/ic_forword_arrow_red.png',
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            color: Color(0xFFF7F7F7),
            margin: EdgeInsets.only(right: 10),
            // padding: EdgeInsets.only(bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10, right: 5),
                  child: Text(
                    'Green Tea',
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontFamily: 'Gotham Rounded',
                        fontStyle: FontStyle.normal),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 4, right: 5),
                  child: Text(
                    'Sold By ABC',
                    style: TextStyle(
                        fontSize: 9,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                        fontFamily: 'Gotham Rounded',
                        fontStyle: FontStyle.normal),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, bottom: 5, top: 6),
                        child: Text(
                          '₹ 790.00/-',
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                              fontFamily: 'Gotham Rounded',
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/images/ic_rectangle_cart.png',
                      height: 27,
                      width: 27,
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    ),
  );
}

Widget welnessMarketProducts(BuildContext ctxt, int index) {
  bool isSelected = false;
  return Container(
      margin: EdgeInsets.fromLTRB(5, 0, 8, 1),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(welnessMarketCategoryList[index].CategoryName,
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                fontFamily: 'Gotham Rounded',
                fontSize: 14,
                color: Color(0xFF333333),
                // decoration: TextDecoration.underline,
              )),
          Container(
            color: Color(0xFFFF9C01),
            width: 100,
            height: 1,
          )
        ],
      ));
}
