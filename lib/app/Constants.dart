class Strings {
  static const String APP_NAME = "Luke Coutinho";
  static const String APP_NAME_CAPS = "LUKE COUTINHO";
  static const String APP_INFO = "Integative & Lifestyle Medicine | Holistic Nutrition";
  static const String HOME_SEARCH_HINT = "Search from Luke's Dictionary";
  static const String HEALTHY_TIPS = "Healthy Tips";
  static const String VIEW_ALL = "View All";
  static const String LUKE_TALKS = "Luke Talks";
  static const String HEALTHY_RECIPIES= "Healthy Recipes";
  static const String LUKE_RECOMMENDS= "Luke Recommends";
  static const String FEATURED_PRODUCTS= "Featured Products";
}