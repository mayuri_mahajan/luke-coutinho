import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:carousel_indicator/carousel_indicator.dart';

import 'app/Constants.dart' as Constants;
import 'model/UserListModel.dart';
import 'ui/pages/welness_market.dart';
import 'ui/widgets/HomeHealthRecipiesListItem.dart';
import 'ui/widgets/HomeLiveUserListItem.dart';
import 'ui/widgets/HomeLukeTalksListItems.dart';
import 'ui/widgets/HomeUserListItem.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white, //or set color with: Color(0xFF0000FF)
      // statusBarBrightness: Brightness.light,
    ));

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.blue,
        // backgroundColor: Color(0xFFF7F7F7),
        fontFamily: 'Gotham Rounded',

/*
          fontFamily: 'Gotham Rounded',
*/
      ),
      home: MyHomePage(),
    );
  }
}

final digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
List<String> pastLiveSessionsUser = [
  "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2",
  "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile2.png?alt=media&token=ca8a6524-dbe1-4edd-9adc-97e5866c4721",
  "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile3.png?alt=media&token=43f0a384-5936-4887-ad99-a1f19ee8ba59",
  "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile4.png?alt=media&token=07255b48-7297-47c3-b690-9bca43e898ca"
];
List<String>tips=['','',''];

List<UserList> userList = [
  new UserList(
      "1",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser1.png?alt=media&token=6f651fdd-b73c-4cfc-b1be-276f18f6e990",
      ""),
  new UserList(
      "2",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser2.png?alt=media&token=74819295-5b0d-4e89-a454-8c247eafc998",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2"),
  new UserList(
      "3",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser2.png?alt=media&token=74819295-5b0d-4e89-a454-8c247eafc998",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2"),
  new UserList(
      "4",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser1.png?alt=media&token=6f651fdd-b73c-4cfc-b1be-276f18f6e990",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2"),
  new UserList(
      "5",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser2.png?alt=media&token=74819295-5b0d-4e89-a454-8c247eafc998",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2"),
  new UserList(
      "6",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fuser2.png?alt=media&token=74819295-5b0d-4e89-a454-8c247eafc998",
      "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fprofile1.png?alt=media&token=e04c55e2-8a5a-4b1d-b81e-95e51ac384b2")
];
int _currentPage=0;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    int _focusedIndex = 0;

    void _onItemFocus(int index) {
      setState(() {
        _focusedIndex = index;
      });
    }

    Widget _buildItemDetail() {
      if (userList.length > _focusedIndex)
        return Container(
          height: 220,
          // child: Text("index $_focusedIndex: ${userList[_focusedIndex]}"),
        );

      return Container(
        height: 200,
        child: Text("No Data"),
      );
    }

    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        //  brightness: Brightness.dark,
        //preferredSize: Size(double.infinity, 56),

        title: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    onTap: OpenNewScreen,
                    child: Image.asset(
                      'assets/images/logo.png',
                      height: 24,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: new Container(
                width: 40.0,
                height: 40.0,
                decoration: new BoxDecoration(
                  color: Color(0xFFFF9C01).withOpacity(0.12),
                  boxShadow: [
                    new BoxShadow(
                        color: Color(0xFFFF9C01).withOpacity(0.2),
                        offset: Offset(0, 6), //(x,y)
                        blurRadius: 12.0)
                  ],
                  borderRadius: new BorderRadius.all(new Radius.circular(40.0)),
                ),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Image.asset(
                    'assets/images/ic_home_calender.png',
                    height: 35,
                    width: 35,
                  ),
                ),
              ),
            ),
            new Container(
              width: 40.0,
              height: 40.0,
              decoration: new BoxDecoration(
                color: Color(0xFFFF9C01).withOpacity(0.12),
                boxShadow: [
                  new BoxShadow(
                      color: Color(0xFFFF9C01).withOpacity(0.2),
                      offset: Offset(0, 6), //(x,y)
                      blurRadius: 12.0)
                ],
                borderRadius: new BorderRadius.all(new Radius.circular(40.0)),
              ),
              child: Container(
                padding: EdgeInsets.all(10),
                child: Image.asset(
                  'assets/images/ic_home_bell.png',
                  height: 35,
                  width: 35,
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        blurRadius: 20.0,
                        offset: Offset(0, 0.1)),
                  ],
                ),
                child: Container(
                  //  padding: EdgeInsets.only(top:10),
                  color: Colors.white,
                  padding: const EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 13.0),
                  child: Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      TextField(
                        cursorColor: Color(0xFF888888),
                        autocorrect: true,
                        maxLines: 1,
                        minLines: 1,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 11, horizontal: 12),
                          /*contentPadding: EdgeInsets.fromLTRB(
                            15.0, 0, 10.0, 0),*/
                          hintText: Constants.Strings.HOME_SEARCH_HINT,
                          fillColor: Colors.white,
                          hintStyle: TextStyle(
                            color: Color(0xFF888888).withOpacity(0.8),
                            fontSize: 13,
                            fontWeight: FontWeight.w100,
                            fontStyle: FontStyle.normal,
                          ),
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                BorderSide(color: Color(0xFF888888), width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.0)),
                            borderSide:
                                BorderSide(color: Color(0xFF888888), width: 1),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(5.0, 0, 14.0, 1),
                        child: Image.asset(
                          'assets/images/ic_search.png',
                          height: 18,
                          width: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 17, 0, 10),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 150.0,
                    maxHeight: 210.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: userList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        buildBody(context, index),
                    // buildBody(context, index),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(12, 5, 12, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          Constants.Strings.HEALTHY_TIPS,
                          style: TextStyle(
                              fontSize: 17,
                              color: Color(0xFF000000),
                              fontFamily: 'Gotham Rounded',
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            Constants.Strings.VIEW_ALL,
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xFFFF9C01),
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/ic_forword_arrow.png',
                              height: 10,
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          ),
                        ],
                      ),
                    ],
                  )),
              Container(
                width: double.infinity,
                height: 250,
                /*padding: EdgeInsets.fromLTRB(10, 10, 10, 5),*/
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                        color: Color(0XFF000000).withOpacity(0.03),
                        offset: Offset(0, 4),
                        //(x,y)
                        blurRadius: 15.0)
                  ],
                ),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      new BoxShadow(
                          color: Color(0XFFCECECE).withOpacity(0.3),
                          offset: Offset(0, 4),
                          //(x,y)
                          blurRadius: 15.0)
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(13, 13, 13, 13),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Container(
                          height: 130.0,
                          margin:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(8.0),
                            color: const Color(0xff000000).withOpacity(0.1),
                          ),
                          child:CarouselSlider(
                              options: CarouselOptions(
                                height: 250,
                                viewportFraction: 1,
                                autoPlayCurve: Curves.fastOutSlowIn,
                                enlargeCenterPage: true,
                                enableInfiniteScroll: true,
                                reverse: false,
                                autoPlay: true,
                                disableCenter: false,
                                autoPlayInterval: Duration(seconds: 3),
                                autoPlayAnimationDuration: Duration(milliseconds: 800),
                                  onPageChanged:(index, reason) {
                                    setState(() {
                                      _currentPage = index;
                                    });
                                  },

                              ),
                              items: tips.map((i) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return CachedNetworkImage(
                                      imageUrl:
                                      'https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fhealthy_tip.png?alt=media&token=246be8d8-3ae2-4964-ae07-26be05a05f66',
                                      imageBuilder: (context, imageProvider) => Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          borderRadius: BorderRadius.circular(8.0),
                                          color: const Color(0xff000000),
                                          image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover,
                                            colorFilter: ColorFilter.mode(
                                                Colors.black.withOpacity(0.7),
                                                BlendMode.dstATop),
                                          ),
                                        ),
                                      ),
                                      // placeholder: (context, url) => CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    );
                                  },
                                );
                              }).toList(),
                          ),
                        ),
                       //   SizedBox(height: 40,),
                          Padding(
                            padding:EdgeInsets.only(top: 120),
                            child: Align(
                              alignment:Alignment.bottomCenter,
                              child: CarouselIndicator(
                                count: tips.length,
                                index: _currentPage,
                                height: 2,
                                cornerRadius: 0,
                                width: 25,
                              ),
                            ),
                          ),
                        ],

                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(9, 12, 8, 5),
                        child: Text(
                          'The Sugar and Cancer Connection',
                          style: TextStyle(
                            fontSize: 17,
                            fontFamily: 'Ubantu',
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(9, 4, 8, 20),
                        child: Text(
                          'This is what you should do to feel energised all day long.',
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Ubantu',
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              /*Container(
                height: 270,
                child: CarouselSlider(
                  options: CarouselOptions(
                    height: 250,
                    viewportFraction: 1,
                  ),
                  items: userList.map((i) {suga
                    return Builder(
                      builder: (BuildContext context) {
                        return helthyTipsListItem(context, userList.indexOf(i));
                      },
                    );
                  }).toList(),
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 20),
                child: Stack(
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(12, 45.0, 12.0, 0),
                      decoration: new BoxDecoration(
                        color: Color.fromRGBO(255, 156, 1, 1),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(80),
                          topLeft: Radius.circular(80),
                        ),
                      ),
                      padding: EdgeInsets.fromLTRB(4.0, 5.0, 2.0, 5.0),
                      width: double.infinity,
                      height: 255,
                    ),
                    Positioned(
                      left: 15,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/images/home_live_workout.png',
                            height: 190,
                            width: 140,
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 15),
                            child: Text(
                              'Past live Sessions',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 7.0),
                            child: Row(
                              children: [
                                ConstrainedBox(
                                  constraints: new BoxConstraints(
                                    minHeight: 43.0,
                                    maxHeight: 43.0,
                                  ),
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    // padding: EdgeInsets.only(left: 3),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: pastLiveSessionsUser.length,
                                    itemBuilder:
                                        (BuildContext context, index) =>
                                            liveUserListItems(context, index),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                  child: new Container(
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [
                                        new BoxShadow(
                                            color: Color(0xFF000000)
                                                .withOpacity(0.2),
                                            offset: Offset(0, 6), //(x,y)
                                            blurRadius: 12.0)
                                      ],
                                      image: new DecorationImage(
                                        image: new NetworkImage(''),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(30.0)),
                                      border: new Border.all(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Image.asset(
                                          'assets/images/ic_forword_arrow_red.png'),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                        top: 60,
                        right: 33,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(11.0),
                          child: Container(
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(
                                vertical: 4, horizontal: 10),
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/images/ic_live.png',
                                  height: 8,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    'LIVE',
                                    style: TextStyle(
                                        color: Color(0xFF1B1B1D),
                                        fontSize: 12,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),
                    Positioned(
                        top: 100,
                        left: 155,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Luke Live',
                                style: TextStyle(
                                  fontSize: 23,
                                  color: Colors.white,
                                  fontFamily: 'Gotham Rounded',
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Text(
                                  'Next Live Session will start in',
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w100,
                                      fontStyle: FontStyle.normal),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 2, 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              4.5,
                                                              4.5,
                                                              4.5,
                                                              4.5),
                                                      margin: EdgeInsets.only(
                                                          right: 5),
                                                      decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: Color(0xFFFFFFFF)
                                                            .withOpacity(0.25),
                                                        /*boxShadow: [

                                                        new BoxShadow(
                                                            offset: Offset(0, 0),
                                                            color: Color(0xFFFFFFFF)
                                                                .withOpacity(0.2),
                                                            //(x,y)0.25
                                                            blurRadius: 1)
                                                      ]*/
                                                      ),
                                                      child: Text(
                                                        '0',
                                                        style: TextStyle(
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18,
                                                          color: Colors.white,
                                                        ),
                                                      )),
                                                  Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              4.5,
                                                              4.5,
                                                              4.5,
                                                              4.5),
                                                      decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: Color(0xFFFFFFFF)
                                                            .withOpacity(0.25),
                                                        /*boxShadow: [

                                                new BoxShadow(
                                                    offset: Offset(0, 0),
                                                    color: Color(0xFFFFFFFF)
                                                        .withOpacity(0.2),
                                                    //(x,y)0.25
                                                    blurRadius: 1)
                                              ]*/
                                                      ),
                                                      child: Text(
                                                        '0',
                                                        style: TextStyle(
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18,
                                                          color: Colors.white,
                                                        ),
                                                      )),
                                                ],
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      top: 5, left: 2),
                                                  child: Text(
                                                    'Hrs.',
                                                    style: TextStyle(
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 12,
                                                      color: Colors.white,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              4.5,
                                                              4.5,
                                                              4.5,
                                                              4.5),
                                                      margin: EdgeInsets.only(
                                                          right: 5),
                                                      decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: Color(0xFFFFFFFF)
                                                            .withOpacity(0.25),
                                                        /*boxShadow: [

                                                        new BoxShadow(
                                                            offset: Offset(0, 0),
                                                            color: Color(0xFFFFFFFF)
                                                                .withOpacity(0.2),
                                                            //(x,y)0.25
                                                            blurRadius: 1)
                                                      ]*/
                                                      ),
                                                      child: Text(
                                                        '0',
                                                        style: TextStyle(
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18,
                                                          color: Colors.white,
                                                        ),
                                                      )),
                                                  Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              4.5,
                                                              4.5,
                                                              4.5,
                                                              4.5),
                                                      decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: Color(0xFFFFFFFF)
                                                            .withOpacity(0.25),
                                                        /*boxShadow: [

                                                new BoxShadow(
                                                    offset: Offset(0, 0),
                                                    color: Color(0xFFFFFFFF)
                                                        .withOpacity(0.2),
                                                    //(x,y)0.25
                                                    blurRadius: 1)
                                              ]*/
                                                      ),
                                                      child: Text(
                                                        '0',
                                                        style: TextStyle(
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18,
                                                          color: Colors.white,
                                                        ),
                                                      )),
                                                ],
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      top: 5, left: 2),
                                                  child: Text(
                                                    'Min.',
                                                    style: TextStyle(
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 12,
                                                      color: Colors.white,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Column(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            4.5, 4.5, 4.5, 4.5),
                                                    margin: EdgeInsets.only(
                                                        right: 5),
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.rectangle,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color: Color(0xFFFFFFFF)
                                                          .withOpacity(0.25),
                                                      /*boxShadow: [

                                                      new BoxShadow(
                                                          offset: Offset(0, 0),
                                                          color: Color(0xFFFFFFFF)
                                                              .withOpacity(0.2),
                                                          //(x,y)0.25
                                                          blurRadius: 1)
                                                    ]*/
                                                    ),
                                                    child: Text(
                                                      '0',
                                                      style: TextStyle(
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18,
                                                        color: Colors.white,
                                                      ),
                                                    )),
                                                Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            4.5, 4.5, 4.5, 4.5),
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.rectangle,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color: Color(0xFFFFFFFF)
                                                          .withOpacity(0.25),
                                                      /*boxShadow: [

                                              new BoxShadow(
                                                  offset: Offset(0, 0),
                                                  color: Color(0xFFFFFFFF)
                                                      .withOpacity(0.2),
                                                  //(x,y)0.25
                                                  blurRadius: 1)
                                            ]*/
                                                    ),
                                                    child: Text(
                                                      '0',
                                                      style: TextStyle(
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18,
                                                        color: Colors.white,
                                                      ),
                                                    )),
                                              ],
                                            ),
                                            Container(
                                                margin: EdgeInsets.only(
                                                    top: 5, left: 2),
                                                child: Text(
                                                  'Sec.',
                                                  style: TextStyle(
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                  ),
                                                )),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              /* Container(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: FlipPanel.builder(
                              itemBuilder: (context, index) => Container(
                                color: Colors.black,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 6.0),
                                child: Text(
                                  '${digits[index]}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 50.0,
                                      color: Colors.white),
                                ),
                              ),
                              itemsCount: digits.length,
                              period: const Duration(milliseconds: 1000),
                              loop: 1,
                            ),
                          )*/
                            ],
                          ),
                        )),
                    Positioned(
                        bottom: 25,
                        right: 10,
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(8),
                              color: Color(0xFF19A3A9),
                              boxShadow: [
                                new BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.3),
                                    offset: Offset(2, 4),
                                    //(x,y)
                                    blurRadius: 4)
                              ]),
                          child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(9),
                              // color: Color(0xFF000000).withOpacity(0.1),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 7, horizontal: 6),
                            child: Text(
                              'WATCH NOW!',
                              style: TextStyle(
                                  fontSize: 9,
                                  color: Colors.white,
                                  fontFamily: 'Ubantu',
                                  fontWeight: FontWeight.w100,
                                  fontStyle: FontStyle.normal),
                            ),
                          ),
                        ))
                  ],
                ),
              ),
              Wrap(crossAxisAlignment: WrapCrossAlignment.end, children: [
                Container(
                  margin: EdgeInsets.only(top: 5, left: 1, right: 1),
                  height: 300,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12, 0, 4, 4),
                              child: Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(15, 8, 20, 15),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(7.0),
                                    color: Color(0XFFFFE7D0),
                                    boxShadow: [
                                      new BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.08),
                                          offset: Offset(0.0, 4),
                                          //(x,y)
                                          blurRadius: 25)
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8),
                                        child: Text(
                                          'Join Dr. Abhay Talwalkar',
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 12),
                                        child: Text(
                                          'Know everything about Homeopathy.',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, bottom: 4),
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Container(
                                            width: 26.0,
                                            height: 26.0,
                                            decoration: new BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                new BoxShadow(
                                                    color: Color.fromRGBO(
                                                        0, 0, 0, 0.08),
                                                    offset: Offset(0.0, 0.5),
                                                    //(x,y)
                                                    blurRadius: 5)
                                              ],
                                              /* image: new DecorationImage(
                                            image: new NetworkImage(''),
                                            fit: BoxFit.cover,
                                          ),*/
                                              borderRadius: new BorderRadius
                                                      .all(
                                                  new Radius.circular(26.0)),
                                              border: new Border.all(
                                                color: Colors.white,
                                                width: 2.0,
                                              ),
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Image.asset(
                                                'assets/images/ic_forword_arrow_red.png',
                                                color: Color(0xFFF26B2A),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12, 4, 4, 0),
                              child: Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(15, 8, 20, 15),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(7.0),
                                    color: Color(0XFFFFDFD0),
                                    boxShadow: [
                                      new BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.08),
                                          offset: Offset(0.0, 4),
                                          //(x,y)
                                          blurRadius: 25)
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8),
                                        child: Text(
                                          'Join Dr. Abhay Talwalkar',
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 12),
                                        child: Text(
                                          'Know everything about Homeopathy.',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, bottom: 4.0),
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Container(
                                            width: 26.0,
                                            height: 26.0,
                                            decoration: new BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                new BoxShadow(
                                                    color: Color.fromRGBO(
                                                        0, 0, 0, 0.08),
                                                    offset: Offset(0.0, 0.5),
                                                    blurRadius: 5),
                                              ],
                                              image: new DecorationImage(
                                                image: new NetworkImage(''),
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius: new BorderRadius
                                                      .all(
                                                  new Radius.circular(26.0)),
                                              border: new Border.all(
                                                color: Colors.white,
                                                width: 2.0,
                                              ),
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Image.asset(
                                                  'assets/images/ic_forword_arrow_red.png'),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(4, 0, 12, 0),
                            child: Container(
                              padding: EdgeInsets.fromLTRB(15, 10, 0, 0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(7.0),
                                color: Color(0XFFCBF2F4),
                                boxShadow: [
                                  new BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.08),
                                      offset: Offset(0.0, 4),
                                      //(x,y)
                                      blurRadius: 25)
                                ],
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Text(
                                      'Join Dr. Abhay Talwalkar',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 10),
                                      child: Text(
                                        'Know everything about Homeopathy.',
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 93),
                                        child: Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Container(
                                            width: 26.0,
                                            height: 26.0,
                                            decoration: new BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                new BoxShadow(
                                                    color: Color.fromRGBO(
                                                        0, 0, 0, 0.08),
                                                    offset: Offset(0.0, 0.5),
                                                    //(x,y)
                                                    blurRadius: 5)
                                              ],
                                              image: new DecorationImage(
                                                image: new NetworkImage(''),
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius: new BorderRadius
                                                      .all(
                                                  new Radius.circular(26.0)),
                                              border: new Border.all(
                                                color: Colors.white,
                                                width: 2.0,
                                              ),
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Image.asset(
                                                'assets/images/ic_forword_arrow_red.png',
                                                color: Color(0xFF0A989F),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Align(
                                          alignment: Alignment.bottomRight,
                                          child: /*Image.network("https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fdoctor.png?alt=media&token=eb5fea83-e7db-4cb5-b47b-bdc5527828d7",height: 150,)*/
                                              CachedNetworkImage(
                                            height: 150,
                                            imageUrl:
                                                "https://firebasestorage.googleapis.com/v0/b/mayuri-352b6.appspot.com/o/luke%20coutinho%2Fhome%2Fdoctor.png?alt=media&token=eb5fea83-e7db-4cb5-b47b-bdc5527828d7",
                                            //  errorWidget: (context, url, error) => Icon(Icons.error),
                                          ),
                                          /*Image.asset(
                                          'assets/images/doctor.png',
                                          height: 150,
                                        )*/
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
              Container(
                  padding: EdgeInsets.fromLTRB(12, 25, 10, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          Constants.Strings.LUKE_TALKS,
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            Constants.Strings.VIEW_ALL,
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xFFFF9C01),
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/ic_forword_arrow.png',
                              height: 10,
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          ),
                        ],
                      ),
                    ],
                  )),
              Container(
                height: 250,
                padding: EdgeInsets.only(left: 3),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: userList.length,
                  itemBuilder: (BuildContext context, int index) =>
                      lukeTalksListItems(context, index),
                ),
              ),

              /* ScrollSnapList(
                  scrollDirection: Axis.horizontal,
                  onItemFocus: _onItemFocus,
                  itemSize: 200,
                  itemBuilder: (BuildContext context, int index) =>
                      lukeTalksListItems(context, index),
                  itemCount: userList.length,
                  // reverse: false,
                ),
              ),
              // _buildItemDetail(),
            ],
          ),*/

              /*ListView.builder(
              shrinkWrap: true,

              itemCount: userList.length,
              itemBuilder: (BuildContext context, int index) =>
                  lukeTalksListItems(context, index),
            ),*/

              Container(
                  padding: EdgeInsets.fromLTRB(12, 15, 12, 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          Constants.Strings.HEALTHY_RECIPIES,
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                              fontFamily: 'Gotham Rounded',
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            Constants.Strings.VIEW_ALL,
                            style: TextStyle(
                              fontSize: 15,
                              color: Color(0xFFFF9C01),
                            ),
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/ic_forword_arrow.png',
                              height: 10,
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                          ),
                        ],
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(12, 10, 0, 10),
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 240.0,
                    maxHeight: 240.0,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: userList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        helthyRecipieListItem(context, index),
                  ),
                ),
              ),
            ]),
      ),
    );
  }

  void OpenNewScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => WelnessMarket()),
    );
  }
}
